import graph_system
import argparse
import uuid


parser = argparse.ArgumentParser()

sub_parsers = parser.add_subparsers(dest="command", required=True)

sub_parsers.add_parser("gen")

arguments = parser.parse_args()


if arguments.command == "gen":
    import pyperclip

    ref_template = "({})"
    sub_template = "[{0}  {0}]"
    template = ref_template

    while True:
        mode_string = input().strip()

        if mode_string == "ref":
            template = ref_template

        elif mode_string == "sub":
            template = sub_template

        _ = graph_system.encode_uuid(uuid.uuid1())
        pyperclip.copy(_)
        print(template.format(_), end=" ")
