import math
import re
import uuid


class Pattern:
    def match(self, parts):
        raise NotImlementedError


class Match:
    def get_matches(self):
        raise NotImlementedError


class UUIDPattern(Pattern):
    def match(self, parts):
        part = next(iter(parts), None)
        if not isinstance(part, uuid.UUID):
            return None

        return (UUIDPatternMatch(part, self), parts[1:])


class UUIDPatternMatch(Match):
    def __init__(self, uuid, pattern):
        super().__init__()

        self.uuid = uuid
        self.pattern = pattern

    def get_matches(self):
        yield self


class RegexPattern(Pattern):
    def __init__(self, object):
        super().__init__()

        self.object = object

        self._pattern = object if isinstance(object, re.Pattern) else re.compile(object)

    def match(self, parts):
        part = next(iter(parts), None)
        if not isinstance(part, str):
            return None

        match = self._pattern.match(part)
        if match is None:
            return None

        return (RegexPatternMatch(match, self), parts[1:])


class RegexPatternMatch(Match):
    def __init__(self, match, regex_pattern):
        super().__init__()

        self.match = match
        self.regex_pattern = regex_pattern

    def get_matches(self):
        yield self


class RepeatingPattern(Pattern):
    def __init__(self, pattern, min_matches=1, max_matches=math.inf):
        super().__init__()

        self.pattern = pattern
        self.min_matches = min_matches
        self.max_matches = max_matches

    def match(self, parts):
        matches = []

        while True:
            tuple = self.pattern.match(parts)
            if tuple is None:
                if len(matches) < self.min_matches:
                    return None

                break

            match, parts = tuple
            matches.append(match)
            if len(matches) == self.max_matches:
                break

        return (RepeatingPatternMatch(matches, self), parts)


class RepeatingPatternMatch:
    def __init__(self, matches, pattern):
        super().__init__()

        self.matches = matches
        self.pattern = pattern

    def get_matches(self):
        yield self

        for match in self.matches:
            yield from match.get_matches()


class SequencePattern(Pattern):
    def __init__(self, patterns):
        super().__init__()

        self.patterns = patterns

    def match(self, parts):
        matches = []

        for pattern in self.patterns:
            tuple = pattern.match(parts)
            if tuple is None:
                return None

            match, parts = tuple
            matches.append(match)

        return (SequencePatternMatch(matches, self), parts)


class SequencePatternMatch:
    def __init__(self, matches, pattern):
        super().__init__()

        self.matches = matches
        self.pattern = pattern

    def get_matches(self):
        yield self

        for match in self.matches:
            yield from match.get_matches()


OPTIONAL_WHITESPACE = RepeatingPattern(
    RegexPattern(r"\s+"), min_matches=0, max_matches=1
)


def parse_parts(parts, pattern):
    tuple = pattern.match(parts)
    if tuple is None:
        return None

    match, parts = tuple
    if len(parts) != 0:
        return None

    return match


class Actor:
    def process(self, parts, uuid):
        raise NotImlementedError
