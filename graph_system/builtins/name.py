import graph_system.actor as g_actor


_ = [
    g_actor.OPTIONAL_WHITESPACE,
    g_actor.UUIDPattern(),
    g_actor.RegexPattern("\s+is\s+called\s+(?P<name>.*?)\s*$"),
]
_PATTERN = g_actor.SequencePattern(_)


class Actor(g_actor.Actor):
    def __init__(self):
        super().__init__()

        self._names = {}

    def process(self, parts, uuid):
        match = g_actor.parse_parts(parts, _PATTERN)
        if match is None:
            return False

        sub_matches = iter(match.get_matches())

        for sub_match in sub_matches:
            if isinstance(sub_match, g_actor.UUIDPatternMatch):
                uuid = sub_match.uuid
                break

        self._names[uuid] = next(sub_matches).match.group("name")
        return True
