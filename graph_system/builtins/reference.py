import graph_system.actor as g_actor
import uuid


class Actor(g_actor.Actor):
    def __init__(self):
        super().__init__()

        self._references = {}

    def process(self, parts, uuid_):
        references = None

        for part in parts:
            if isinstance(part, uuid.UUID):
                if references is None:
                    references = set()
                    self._references[uuid_] = references

                references.add(part)

        return references is not None
