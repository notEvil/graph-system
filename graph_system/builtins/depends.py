import graph_system.actor as g_actor


_UUID_PATTERN = g_actor.UUIDPattern()
_ = g_actor.SequencePattern([g_actor.RegexPattern("\s*(,|and)\s+"), _UUID_PATTERN])
_ = [
    g_actor.OPTIONAL_WHITESPACE,
    _UUID_PATTERN,
    g_actor.RegexPattern("\s+depends\s+on\s+"),
    _UUID_PATTERN,
    g_actor.RepeatingPattern(_, min_matches=0),
    g_actor.OPTIONAL_WHITESPACE,
]
_PATTERN = g_actor.SequencePattern(_)


class Actor(g_actor.Actor):
    def __init__(self):
        super().__init__()

        self._dependencies = {}

    def process(self, parts, uuid):
        match = g_actor.parse_parts(parts, _PATTERN)
        if match is None:
            return False

        sub_matches = iter(match.get_matches())

        for sub_match in sub_matches:
            if isinstance(sub_match, g_actor.UUIDPatternMatch):
                uuid = sub_match.uuid
                break

        dependencies = self._dependencies.setdefault(uuid, set())

        for sub_match in sub_matches:
            if not isinstance(sub_match, g_actor.UUIDPatternMatch):
                continue

            dependencies.add(sub_match.uuid)

        return True
