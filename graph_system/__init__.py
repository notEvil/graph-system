import base64
import os.path as o_path
import re
import uuid


def encode_uuid(uuid):
    return base64.b32encode(uuid.bytes)[:26].decode().lower()


def decode_uuid(string):
    return uuid.UUID(bytes=base64.b32decode(string.upper().encode() + b"======"))


_UUID_PATTERN = r"[a-z2-7]{26}"
_REF_PATTERN = r"\({}\)".format(_UUID_PATTERN)
_SUB_PATTERN = r"(?:\[|\s)?{}(?:\]|\s)?".format(_UUID_PATTERN)
_PATTERN = "(?P<ref>{})|(?P<sub>{})".format(_REF_PATTERN, _SUB_PATTERN)


def parse_string(string):
    parts_dictionary = {}
    stack = []
    uuid_ = None
    parts = []
    index = 0

    for match in re.finditer(_PATTERN, string):
        ref_string = match.group("ref")
        if ref_string is None:
            sub_string = match.group("sub")
            if not (sub_string.startswith("[") ^ sub_string.endswith("]")):
                continue

        start_index = match.start()
        if index < start_index:
            _add_string(string[index:start_index], parts)

        if ref_string is not None:
            parts.append(decode_uuid(ref_string[1:27]))

        else:
            if sub_string.startswith("["):
                sub_uuid = decode_uuid(sub_string[1:27])
                stack.append((uuid_, parts))
                uuid_ = sub_uuid
                parts = []

            else:
                assert decode_uuid(sub_string[-27:-1]) == uuid_

                assert uuid_ not in parts_dictionary.keys()
                parts_dictionary[uuid_] = parts
                uuid_, parts = stack.pop()

        index = match.end()

    assert len(stack) == 0

    if index < len(string):
        _add_string(string[index:], parts)

    parts_dictionary[None] = parts

    return parts_dictionary


def _add_string(string, parts):
    if len(parts) != 0 and isinstance(parts[-1], str):
        parts[-1] += string

    else:
        parts.append(string)


def crop_parts(parts):
    return crop_left(list(crop_bottom(crop_top(parts))))


def crop_top(parts):
    parts = iter(parts)
    match = None

    for part in parts:  # first part
        if isinstance(part, str):
            match = re.search(r"^([ \t]*\n)+", part)  # empty lines, except last
            if match is None:
                if re.search(r"[^ \t]", part) is not None:  # special case last
                    yield part

            elif match.end() != len(part):
                string = part[match.end() :]
                for part in parts:  # second part
                    yield string
                    yield part
                    break

                else:  # no second part
                    if re.search(r"[^ \t]", string) is not None:  # special case last
                        yield string

        else:
            yield part

        break

    yield from parts


def crop_bottom(parts):
    parts = iter(parts)

    for previous_part in parts:
        break

    else:
        return

    for part in parts:
        yield previous_part
        previous_part = part

    if isinstance(previous_part, str):
        match = re.search(r"((^|\n)[ \t]*)+$", previous_part)
        if match is None:
            yield previous_part

        elif match.start() != 0:
            yield previous_part[: match.start()]

    else:
        yield previous_part


def crop_left(parts):
    prefix = _get_prefix(parts)
    if prefix == "":
        yield from parts
        return

    first = True
    pattern = r"^{}".format(re.escape(prefix))
    repl = lambda match: match.group(0) if match.start() == 0 and not first else ""

    for part in parts:
        if isinstance(part, str):
            string, _ = re.subn(pattern, repl, part, flags=re.MULTILINE)
            if string != "":
                yield string

        else:
            yield part

        first = False


def _get_prefix(parts):
    return o_path.commonprefix(list(_get_prefixes(parts)))


def _get_prefixes(parts):
    pattern = r"^(?P<prefix>[ \t]*)(?P<end>\S|\Z)"
    match = None
    first = True
    newline = True

    for part in parts:
        if isinstance(part, str):
            for match in re.finditer(pattern, part, re.MULTILINE):
                if match.start() == 0 and not first:  # special case for first string
                    match = None
                    continue

                if match.group("end") != "":  # immediately valid
                    yield match.group("prefix")
                    match = None

            newline = part.endswith("\n")

        else:
            if match is not None:  # delayed valid
                yield match.group("prefix")
                match = None

            elif newline:
                yield ""

            newline = False

        first = False
