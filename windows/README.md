**Caveat: the following is largely subjective and might not be true. Please try to derive your own conclusions based on multiple sources!**

# Windows support

... is complicated. Unlike modern operating systems, it doesn't promote\* software package management and instead relies on the user to find the right and trusted installer, run it with the right privileges and keep the software up-to-date. Because this is tedious work, most software is self-contained and some update themselves. This has certain advantages (e.g. doesn't require dependency resolution) and disadvantages (e.g. they become large) but ultimatively is the easier way to go on Windows. By now, Windows users expect software to be installed by a single installer with a graphical user interface (GUI).

Actually, Windows users expect a GUI for everything. A terminal with black background is so rare that it immediately looks suspicious. Unfortunately, most Python programs rely on the terminal for user interaction which usually is totally fine.

Already for these reasons it isn't straightforward to support Windows without major inconveniences in both development and user experience. Please keep this in mind when you start your rant about this stupid Linux garbage.

\* In my struggles I found winget and WingetUI, among others. Wonder why winget doesn't have an official GUI and isn't integrated with the Software section of Windows Settings.