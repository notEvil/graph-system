import os
import traceback


def run(function):
    if os.environ.get("_NO_SCRIPT", "0") != "0":
        return function()

    try:
        function()

    except:
        traceback.print_exc()
        raise

    finally:
        print("Press Enter to exit")
        input()
