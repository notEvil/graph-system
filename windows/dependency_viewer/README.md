see [Windows support](../README.md)

# Dependency viewer on Windows

## Quickstart

- install the latest version of Python (e.g. `winget install "Python 3"` or from [https://www.python.org/](https://www.python.org/))
- download this repository
- navigate to this directory
- execute or double click `prepare.py`
    - it will install [pipenv](https://github.com/pypa/pipenv) and create a virtual environment
- now you can open any file with `run.bat` and it will create a file of the same name + `.html`

## Quickstop

- navigate to this directory
- execute or double click `remove.py`