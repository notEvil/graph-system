import _script


def _():
    import pathlib
    import subprocess

    if pathlib.Path("./Pipfile").exists():
        code = subprocess.Popen(["pipenv", "--venv"]).wait()
        assert code in [0, 1]

        if code == 0:
            assert subprocess.Popen(["pipenv", "--rm"]).wait() == 0

    for path_string in ["./Pipfile.lock", "./Pipfile"]:
        path = pathlib.Path(path_string)
        if path.exists():
            path.unlink()


_script.run(_)
