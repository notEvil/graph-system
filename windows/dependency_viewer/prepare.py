import _script


def _():
    import os
    import pathlib
    import shutil
    import subprocess
    import sys

    if shutil.which("pipenv") is None:
        assert subprocess.Popen(["pip", "install", "pipenv"]).wait() == 0

    _ = os.environ | dict(_NO_SCRIPT="1")
    assert subprocess.Popen([sys.executable, "./remove.py"], env=_).wait() == 0

    pathlib.Path("./Pipfile").touch()
    assert subprocess.Popen(["pipenv", "install"]).wait() == 0


_script.run(_)
