import _script


def _():
    import pathlib
    import sys
    import subprocess

    PATH = pathlib.Path(__file__).parent

    (path,) = sys.argv[1:]
    path = pathlib.Path(path)

    _ = [
        "pipenv",
        "run",
        "python",
        str(PATH / ".." / ".." / "dependency_viewer.py"),
        "-i",
        str(path),
        "-o",
        str(path) + ".html",
        "--no-compile",
    ]
    assert subprocess.Popen(_).wait() == 0


_script.run(_)
