# Graph System

*Graph System*, first and foremost, is a concept.
Currently, it is **not** fully **specified**, **not** in the least **implemented**, **hardly useful** and maybe a **bad idea** after all.

## Motivation

I got frustrated with reality.
We create and work with tools that enforce a linear structure.
In the physical world, this makes perfect sense since there are only 3 dimensions.
In the digital world however, we are not bound to only three.
We can represent things and relations as a graph and traverse this graph in a non-linear manner.
Consider reading Wikipedia: you read about a topic and may follow links to related topics, or to the same topic in different languages.
There are no 3 dimensional arrangements of Wikipedia.
Obviously, text on a page is still linear, but I would argue that some level of linearity is actually desirable.

To be specific, I frequently wonder why I have to manage files in a file system (where, what names) when all I need is a set of files with content.
The individual files would be easily identifiable by their content and their relations to other files.
Another example: why is there no easy way to write text the way we think.
The thought process is inherently non-linear, sometimes circular, and therefore difficult to write down in linear form.

Problems of this kind may be solved each by some application, but I really don't want to look for and learn an application whenever I find myself limited by standard tools.
I may be wrong, but I think that a simple definition of a graph and a simple way to modify a graph, e.g.
by just writing text, combined with common tools to work with it, e.g.
search and visualize, would make a significant difference.

## Concept

*Please forgive me for the level of abstraction!*

- Everything is a node in a universal graph
- Anything may refer to anything
- References aren't anything (no recursion)

### Representation

- References have a textual or binary representation
- Everything has a textual or binary representation potentially containing textual or binary representations of references

### Realization

- Everything has a unique identifier
- Unique identifiers aren't anything (no recursion)
- Unique identifiers have a textual or binary representation

## Design

### Textual and binary representation

- References

  `({target identifier})`

- Unique identifiers

  Native encoding

### Nested representation

- Nested representations are necessary for Graph System to be practical
- A sub representation starts with `[{unique identifier}{space}` and ends with `{space}{unique identifier}]`

## Comparison with common file systems

Assuming usual usage patterns:

- Not everything is a file
  - (would be impractical)
- Paths
  - (form a tree)
  - (are used for references)
  - are not unique identifiers
- Unique identifiers of files are either hidden or not used

## Comparison with common graph databases

## Current implementation

### Integration

- Vim

```vim
syntax match gsConceilRef "\v\([a-z2-7]{4}\zs[a-z2-7]{22}\)" conceal cchar=)
syntax match gsConceilSub1 "\v\[[a-z2-7]{4}\zs[a-z2-7]{22}\ze(\s|$)" conceal
syntax match gsConceilSub2 "\v(^|\s)[a-z2-7]{4}\zs[a-z2-7]{22}\]" conceal cchar=]
set conceallevel=2
```
truncates UUIDs to 4 characters

## Goals

## Challenges

