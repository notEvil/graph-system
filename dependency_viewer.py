import graph_system
import graph_system.builtins.depends as gb_depends
import graph_system.builtins.name as gb_name
import graph_system.builtins.reference as gb_reference
import argparse
import base64
import hashlib
import json
import pathlib
import subprocess
import sys
import tempfile
import uuid


parser = argparse.ArgumentParser()

parser.add_argument("-i", default="-")
parser.add_argument("-o", default="-")
parser.add_argument("--no-compile", default=False, action="store_true")

arguments = parser.parse_args()


if arguments.i == "-":
    string = sys.stdin.read()

else:
    with open(arguments.i, "r") as file:
        string = file.read()

parts_dictionary = graph_system.parse_string(string)

reference_actor = gb_reference.Actor()
dependency_actor = gb_depends.Actor()
name_actor = gb_name.Actor()
for uuid_, parts in parts_dictionary.items():
    reference_actor.process(parts, uuid_)
    name_actor.process(parts, uuid_) or dependency_actor.process(parts, uuid_)


out_file = sys.stdout if arguments.o == "-" else open(arguments.o, "w")

_ = """
<!DOCTYPE html><head>
<style title="t">
  html * {
    font-family: monospace;
  }

  div.node {
    white-space: break-spaces;
    background-color: white;
  }

  span.ref {
    display: inline-flex;
    flex-direction: row;
  }

  div.sub {
    border-left: 1px solid black;
    border-right: 1px solid black;
    padding-left: 1ch;
    padding-right: 1ch;
  }
</style>
</head><body style="margin-top: 0;">
  <div style="position: sticky; top: 0; padding: 8px; background-color: white;">
    <button id="expand">expand all</button> <button id="collapse">collapse all</button> <button id="leafs">show leafs</button> <button id="side">content right</button><br>
  </div>
  <br>
""".lstrip()
print(_, file=out_file)


def visit(uuid):
    for referenced_uuid in reference_actor._references.get(uuid, []):
        if referenced_uuid in referenced_uuids:
            continue

        referenced_uuids.add(referenced_uuid)
        visit(referenced_uuid)


referenced_uuids = set()
visit(None)

for uuid_, parts in parts_dictionary.items():
    if not (uuid_ is None or uuid_ in referenced_uuids):
        continue

    _ = "<div class='node' data-uuid='{}'{}>".format(
        "None" if uuid_ is None else graph_system.encode_uuid(uuid_),
        "" if uuid_ is None else ' style="display:none;"',
    )
    print(_, end="", file=out_file)

    for part in graph_system.crop_parts(parts):
        if isinstance(part, str):
            print(part, end="", file=out_file)

        elif isinstance(part, uuid.UUID):
            uuid_string = graph_system.encode_uuid(part)
            name = name_actor._names.get(part)
            if name is None:
                name = uuid_string[:4]
            _ = "<span class='ref' data-uuid='{0}'><a class='uuid' title='{0}' href='#'>({1})</a></span>"
            print(_.format(uuid_string, name), end="", file=out_file)

        else:
            raise NotImplementedError

    print("</div>", file=out_file)


javascript_code = """
var hover_uuid = null
var clicked_uuid = null
var leafs = false
var leafs_element = document.getElementById('leafs')
var fragile_uuids = []
var side_element = document.getElementById('side')

function on_reference_mouseover(event) {
    hover_uuid = get_uuid(event.target)
    update_hover_colors(hover_uuid)
    event.stopPropagation()
}

function on_reference_mouseout(event) {
    var uuid = hover_uuid
    hover_uuid = null
    update_hover_colors(uuid)
    event.stopPropagation()
}

function update_hover_colors(uuid) {
    update_reference_colors(uuid)
    for (var dependency_uuid of get(dependency_uuids, uuid, [])) {
        update_reference_colors(dependency_uuid)
    }
    for (var dependent_uuid of get(dependent_uuids, uuid, [])) {
        update_reference_colors(dependent_uuid)
    }
    update_reference_colors(clicked_uuid)
    for (var dependency_uuid of get(dependency_uuids, clicked_uuid, [])) {
        update_reference_colors(dependency_uuid)
    }
    for (var dependent_uuid of get(dependent_uuids, clicked_uuid, [])) {
        update_reference_colors(dependent_uuid)
    }
    for (var fragile_uuid of fragile_uuids) {
        update_reference_colors(fragile_uuid)
    }
}

function on_uuid_click(event) {
    var reference = event.target.parentNode
    var sub = get_element(reference, 'sub')
    if (sub === null) {
        var uuid = reference.dataset['uuid']
        sub = nodes[uuid].cloneNode(true)
        sub.removeAttribute('id')
        sub.classList.add('sub')
        sub.style.display = ''
        if (uuid === hover_uuid) {
            set_background_color(sub, 'lightgray')
        }
        for (var sub_reference of get_references(undefined, sub)) {
            get_element(sub_reference, 'uuid').addEventListener('click', on_uuid_click)
            sub_reference.addEventListener('mouseover', on_reference_mouseover)
            sub_reference.addEventListener('mouseout', on_reference_mouseout)
        }
        sub.addEventListener('click', on_sub_click)
        reference.appendChild(sub)
    } else {
        sub.style.display = is_display_none(sub) ? '' : 'none'
    }
    event.stopPropagation()
}

function on_expand(event) {
    get_references(undefined, undefined).filter((reference) => is_target(reference, false) && !is_cyclic_reference(reference)).forEach(click_reference)
}

function on_collapse(event) {
    get_references(undefined, undefined).filter((reference) => is_target(reference, true)).forEach(click_reference)
}

function is_target(reference, collapse) {
    if (is_hidden(reference) || !(!is_expanded(reference) ^ collapse)) {
        return false
    }
    if (collapse) {
        var sub = get_element(reference, 'sub')
        if (sub !== null) {
            for (var sub_reference of get_references(undefined, sub)) {  // could be too expensive, shallow search?
                if (!is_hidden(sub_reference) && is_expanded(sub_reference)) {
                    return false
                }
            }
        }
    }
    return true
}

function is_hidden(reference) {
    var element = reference
    while (element !== null) {
        if (is_display_none(element)) {
            return true
        }
        element = element.parentNode
    }
    return false
}

function is_expanded(reference) {
    var sub = get_element(reference, 'sub')
    if (sub === null) {
        return false
    }
    return !is_display_none(sub)
}

function is_display_none(element) {
    return element.style !== undefined && element.style.display == 'none'
}

function is_cyclic_reference(reference) {
    var uuid = reference.dataset['uuid']
    var element = reference.parentNode
    while (element !== null) {
        if (is_class(element, 'node') && element.dataset['uuid'] == uuid) {
            return true
        }
        element = element.parentNode
    }
    return false
}

function click_reference(reference) {
    get_element(reference, 'uuid').click()
}

function on_toggle_leafs(event) {
    leafs = !leafs
    leafs_element.textContent = leafs ? 'hide leafs' : 'show leafs'
    fragile_uuids.length = 0
    if (leafs && clicked_uuid !== null) {
        set_fragile(clicked_uuid, {})
    }
    update_reference_colors(undefined)
}

function update_reference_colors(uuid) {
    get_references(uuid, undefined).forEach(update_reference_color)
}

function update_reference_color(reference) {
    var uuid = reference.dataset['uuid']
    var color = ''
    if (leafs && dependency_uuids[uuid] !== undefined && dependent_uuids[uuid] === undefined) {
        color = 'lightblue'
    }
    var selected_uuid = hover_uuid !== null ? hover_uuid : clicked_uuid
    if (selected_uuid === null) {
    } else if (uuid == selected_uuid) {
        color = 'lightgray'
    } else if (hover_uuid === null && fragile_uuids.includes(uuid)) {
        color = '#ddde6b'
    } else if (get(dependency_uuids, selected_uuid, []).includes(uuid)) {
        color = 'lightgreen'
    } else if (get(dependent_uuids, selected_uuid, []).includes(uuid)) {
        color = 'lightsalmon'
    }
    set_background_color(get_element(reference, 'uuid'), color)
    var sub = get_element(reference, 'sub')
    if (sub !== null) {
        set_background_color(sub, color)
    }
}

function set_background_color(element, color) {
    element.style.backgroundColor = color
}

function on_sub_click(event) {
    fragile_uuids.length = 0
    var uuid = get_uuid(event.target)
    if (clicked_uuid !== null && uuid == clicked_uuid) {
        clicked_uuid = null
    } else {
        clicked_uuid = uuid
        if (leafs) {
            set_fragile(uuid, {})
        }
    }
    event.stopPropagation()
}

function get_uuid(element) {
    while (!(is_class(element, 'ref') || is_class(element, 'node'))) {
        element = element.parentNode
    }
    return element.dataset['uuid']
}

function set_fragile(uuid, offsets) {
    fragile_uuids.push(uuid)
    for (var dependency_uuid of get(dependency_uuids, uuid, [])) {
        var offset = get(offsets, dependency_uuid, 0) + 1
        offsets[dependency_uuid] = offset
        if ((dependent_uuids[dependency_uuid].length - offset) == 0) {
            set_fragile(dependency_uuid, offsets)
        }
    }
}

function get(object, key, default_) {
    var value = object[key]
    return value === undefined ? default_ : value
}

function on_side_click(event) {
    var stylesheets = document.styleSheets
    for (const stylesheet of (stylesheets === null ? [] : Array.from(stylesheets))) {
        if (stylesheet.title == 't') {
            for (var rule of stylesheet.cssRules) {
                if (rule.selectorText == 'span.ref') {
                    var left = rule.style.flexDirection == 'row'
                    side_element.textContent = left ? 'content left' : 'content right'
                    rule.style.flexDirection = left ? 'row-reverse' : 'row'
                    break
                }
            }
        }
    }
}

var dependency_uuids = {"DEPENDENCY_UUIDS": []}

var dependent_uuids = {}
for (var [uuid, uuids] of Object.entries(dependency_uuids)) {
  for (var dependency_uuid of uuids) {
    uuids = dependent_uuids[dependency_uuid]
    if (uuids === undefined) {
      uuids = []
      dependent_uuids[dependency_uuid] = uuids
    }
    uuids.push(uuid)
  }
}

var nodes = {}
for (var node of get_nodes(undefined, undefined)) {
    nodes[node.dataset['uuid']] = node
}

document.getElementById('expand').addEventListener('click', on_expand)
document.getElementById('collapse').addEventListener('click', on_collapse)
leafs_element.addEventListener('click', on_toggle_leafs)
side_element.addEventListener('click', on_side_click)

for (var reference of get_references(undefined, undefined)) {
    get_element(reference, 'uuid').addEventListener('click', on_uuid_click)
    reference.addEventListener('mouseover', on_reference_mouseover)
    reference.addEventListener('mouseout', on_reference_mouseout)
}

function get_element(element, class_) {
    for (var sub_element of element.children) {
        if (is_class(sub_element, class_)) {
            return sub_element
        }
    }
    return null
}

function is_class(element, class_) {
    return element.classList !== undefined && element.classList.contains(class_)
}

function get_nodes(uuid, element) {
    return get_elements(`.//div[contains(@class, "node")${uuid === undefined ? '' : ` and @data-uuid='${uuid}'`}]`, element)
}

function get_references(uuid, element) {
    return get_elements(`.//span[contains(@class, "ref")${uuid === undefined ? '' : ` and @data-uuid='${uuid}'`}]`, element)
}

function get_elements(xpath, element) {
    if (element === undefined) {
        element = document
    }

    var iterator = document.evaluate(xpath, element, null, XPathResult.UNORDERED_NODE_ITERATOR_TYPE, null)
    var elements = []

    element = iterator.iterateNext()
    while (element) {
        elements.push(element)
        element = iterator.iterateNext()
    }

    return elements
}
""".lstrip()

_ = graph_system.encode_uuid
dependency_uuids = {
    _(uuid_): [_(uuid_) for uuid_ in uuids]
    for uuid_, uuids in dependency_actor._dependencies.items()
}
dependencies_json = json.dumps(dependency_uuids, separators=(",", ":"))

if arguments.no_compile:
    _ = '{"DEPENDENCY_UUIDS": []}'
    javascript_code = javascript_code.replace(_, dependencies_json)

else:
    hasher = hashlib.sha3_256()
    hasher.update(javascript_code.encode())
    hash = base64.urlsafe_b64encode(hasher.digest()).decode()

    path = pathlib.Path(tempfile.gettempdir(), "dependency_viewer.{}".format(hash))
    if path.exists():
        with open(path, "r") as file:
            javascript_code = file.read()

    else:
        _ = subprocess.PIPE
        stdout, stderr = subprocess.Popen(
            ["closure-compiler", "-O", "ADVANCED"], stdin=_, stdout=_, stderr=_
        ).communicate(javascript_code.encode())
        assert stderr.decode().strip() == "The compiler is waiting for input via stdin."
        javascript_code = stdout.decode()

        with open(path, "w") as file:
            file.write(javascript_code)

    _ = "{DEPENDENCY_UUIDS:[]}"
    javascript_code = javascript_code.replace(_, dependencies_json)

print("""<script>{}</script></body></html>""".format(javascript_code), file=out_file)

out_file.close()
